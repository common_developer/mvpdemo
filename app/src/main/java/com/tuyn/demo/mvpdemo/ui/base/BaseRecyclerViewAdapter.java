package com.tuyn.demo.mvpdemo.ui.base;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class BaseRecyclerViewAdapter<T> extends RecyclerView.Adapter<BaseViewHolder<T>> {
    private final ViewTypeProvider<T> viewTypeProvider;
    private final ViewHolderFactory<T> tViewHolderFactory;
    private List<T> mDataset;
    private BaseViewHolderClickListener<T> mListener;

    // Provide a suitable constructor (depends on the kind of dataset)
    public BaseRecyclerViewAdapter(List<T> myDataset,
                                   ViewTypeProvider<T> viewTypeProvider,
                                   ViewHolderFactory<T> tViewHolderFactory,
                                   BaseViewHolderClickListener<T> listener) {
        if (myDataset==null){
            myDataset = new ArrayList<>(0);
        }
        mListener = listener;
        mDataset = myDataset;
        this.viewTypeProvider = viewTypeProvider;
        this.tViewHolderFactory = tViewHolderFactory;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return tViewHolderFactory.createViewHolder(viewType, parent, mListener);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.bindViewHolder(getItem(position));
    }

    @Override
    public int getItemViewType(int position) {
        return viewTypeProvider.provideViewType(getItem(position));
    }

    private T getItem(int position) {
        return mDataset.get(position);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void addItem(T item){
        mDataset.add(item);
        int index = mDataset.indexOf(item);
        notifyItemInserted(index);
    }
}