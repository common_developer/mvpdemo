package com.tuyn.demo.mvpdemo.ui.fragment.fourth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tuyn.demo.mvpdemo.R;
import com.tuyn.demo.mvpdemo.model.DemoLeftRightModel;
import com.tuyn.demo.mvpdemo.ui.base.BaseFragment;
import com.tuyn.demo.mvpdemo.ui.base.BaseRecyclerViewAdapter;
import com.tuyn.demo.mvpdemo.ui.base.BaseViewHolderClickListener;
import com.tuyn.demo.mvpdemo.ui.error.BaseUIError;
import com.tuyn.demo.mvpdemo.ui.factory.DemoLeftRightViewHolderFactoryImpl;
import com.tuyn.demo.mvpdemo.ui.provider.DemoLeftRightTypeProviderImpl;
import com.tuyn.demo.mvpdemo.ui.util.SoftKeyboardStateWatcher;

public class FourthFragment extends BaseFragment implements  FourthContract.View {

    FourthContract.Presenter mPresenter;
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    DemoLeftRightViewHolderFactoryImpl tViewHolderFactory;
    DemoLeftRightTypeProviderImpl viewTypeProvider;
    BaseRecyclerViewAdapter<DemoLeftRightModel> mAdapter;
    EditText messageEditText;
    Button sendMessageBtn;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = view.findViewById(R.id.recycler);
        mRecyclerView.setHasFixedSize(true);

        messageEditText = view.findViewById(R.id.edittext_chatbox);

        sendMessageBtn = view.findViewById(R.id.button_chatbox_send);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        tViewHolderFactory = new DemoLeftRightViewHolderFactoryImpl(getContext());
        viewTypeProvider = new DemoLeftRightTypeProviderImpl();

        BaseViewHolderClickListener<DemoLeftRightModel> lst = new BaseViewHolderClickListener<DemoLeftRightModel>() {
            @Override
            public void onClick(DemoLeftRightModel item) {
                Log.d("LEFT", "" + item.left);
            }
        };

        mAdapter = new BaseRecyclerViewAdapter<DemoLeftRightModel>(DemoLeftRightModel.getDummyList(), viewTypeProvider, tViewHolderFactory, lst);


        mRecyclerView.setAdapter(mAdapter);

        SoftKeyboardStateWatcher softKeyboardStateWatcher
                = new SoftKeyboardStateWatcher(this.getView());

        softKeyboardStateWatcher.addSoftKeyboardStateListener(new SoftKeyboardStateWatcher.SoftKeyboardStateListener() {
            @Override
            public void onSoftKeyboardOpened(int keyboardHeightInPx) {
                scrollToEnd();
            }

            @Override
            public void onSoftKeyboardClosed() {

            }
        });

        sendMessageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

        mPresenter.create();
    }

    @Override
    public void onDestroyView() {
        mPresenter.destroy();
        super.onDestroyView();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_fourth; //Set fragment layout resource
    }

    @Override
    public void showError(BaseUIError error) {

    }

    @Override
    public void dismissDialog() {

    }

    @Override
    public void messageSent(String message) {
        mAdapter.addItem(new DemoLeftRightModel("aa", "bb", message, false));
        scrollToEnd();
        messageEditText.setText("");
    }

    @Override
    public void setPresenter(FourthContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public boolean isActive() {
        return isResumed();
    }

    private void scrollToEnd(){
        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    private void sendMessage(){
        String message =  messageEditText.getText().toString();
        mPresenter.sendMessage(message);
    }
}
