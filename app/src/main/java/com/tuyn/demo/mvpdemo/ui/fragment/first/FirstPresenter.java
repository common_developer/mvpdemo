package com.tuyn.demo.mvpdemo.ui.fragment.first;

import android.os.Bundle;
import android.support.annotation.NonNull;

public class FirstPresenter implements FirstContract.Presenter {


    private final FirstContract.View mMainView;

    public FirstPresenter(@NonNull FirstContract.View mainView) {
        this.mMainView = mainView;
    }

    @Override
    public void create() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void saveInstanceState(Bundle bundle) {

    }

    @Override
    public void restoreInstanceState(Bundle bundle) {

    }

    @Override
    public void getAllData() {

    }
}