package com.tuyn.demo.mvpdemo.ui.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.tuyn.demo.mvpdemo.R;
import com.tuyn.demo.mvpdemo.ui.base.BaseActivity;
import com.tuyn.demo.mvpdemo.ui.adapter.DemoCollectionPagerAdapter;
import com.tuyn.demo.mvpdemo.ui.base.BaseFragment;
import com.tuyn.demo.mvpdemo.ui.fragment.first.FirstFragment;
import com.tuyn.demo.mvpdemo.ui.fragment.first.FirstPresenter;
import com.tuyn.demo.mvpdemo.ui.fragment.second.SecondFragment;
import com.tuyn.demo.mvpdemo.ui.fragment.second.SecondPresenter;
import com.tuyn.demo.mvpdemo.ui.fragment.third.ThirdFragment;
import com.tuyn.demo.mvpdemo.ui.fragment.third.ThirdPresenter;

public class MainActivity extends BaseActivity  {

    DemoCollectionPagerAdapter mDemoAdapter;
    ViewPager mDemoViewPager;
    TabLayout mDemoTabLayout;
    DrawerLayout mDemoNavigationDrawer;
    NavigationView mDemoNavigationView;
    MainScreenNavigator mDemoMainNavigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        mDemoNavigationDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDemoNavigationDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDemoNavigationDrawer.addDrawerListener(toggle);
        toggle.syncState();

        mDemoNavigationView = findViewById(R.id.nav_view);

        mDemoViewPager = findViewById(R.id.pager);

        mDemoTabLayout = findViewById(R.id.tabs);

        mDemoMainNavigator = new MainScreenNavigator(this, mDemoNavigationView, mDemoViewPager, mDemoTabLayout);

        if (savedInstanceState == null) {
            mDemoMainNavigator.start();
        } else {
            mDemoMainNavigator.update();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public void onNavigationUpdated() {
        mDemoNavigationDrawer.closeDrawer(mDemoNavigationView);
    }

    public void providePresenter(BaseFragment fragment) {
        if(fragment instanceof FirstFragment){
            ((FirstFragment) fragment).setPresenter(new FirstPresenter((FirstFragment) fragment));
        }else if(fragment instanceof SecondFragment){
            ((SecondFragment) fragment).setPresenter(new SecondPresenter((SecondFragment) fragment));
        }else if(fragment instanceof ThirdFragment){
            ((ThirdFragment) fragment).setPresenter(new ThirdPresenter((ThirdFragment) fragment));
        }
    }
}
