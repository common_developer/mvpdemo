package com.tuyn.demo.mvpdemo.ui.adapter;


import android.support.v4.app.Fragment;

import com.tuyn.demo.mvpdemo.ui.fragment.first.FirstFragment;
import com.tuyn.demo.mvpdemo.ui.fragment.first.FirstPresenter;
import com.tuyn.demo.mvpdemo.ui.fragment.second.SecondFragment;
import com.tuyn.demo.mvpdemo.ui.fragment.second.SecondPresenter;
import com.tuyn.demo.mvpdemo.ui.fragment.third.ThirdFragment;
import com.tuyn.demo.mvpdemo.ui.fragment.third.ThirdPresenter;

public class DemoPagerFragmentProvider {
    public static final int FRAG_COUNT = 3;

    public Fragment getItem(int index) {
        Fragment f = new Fragment();
        switch (index) {
            case 0:
                f = provideFirstFragment();
                break;
            case 1:
                f = provideSecondFragment();
                break;
            case 2:
                f = provideThirdFragment();
                break;
        }
        return f;
    }

    private Fragment provideFirstFragment(){
        FirstFragment f = new FirstFragment();
        FirstPresenter p = new FirstPresenter(f);
        f.setPresenter(p);
        return f;
    }

    private Fragment provideSecondFragment(){
        SecondFragment f = new SecondFragment();
        SecondPresenter p = new SecondPresenter(f);
        f.setPresenter(p);
        return f;
    }

    private Fragment provideThirdFragment(){
        ThirdFragment f = new ThirdFragment();
        ThirdPresenter p = new ThirdPresenter(f);
        f.setPresenter(p);
        return f;
    }

    public int getFragmentCount() {
        return FRAG_COUNT;
    }

}
