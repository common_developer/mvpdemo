package com.tuyn.demo.mvpdemo.ui.view.firstactivity;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.tuyn.demo.mvpdemo.ui.adapter.DemoCollectionPagerAdapter;
import com.tuyn.demo.mvpdemo.ui.base.BaseFragment;
import com.tuyn.demo.mvpdemo.ui.base.BaseNavigator;
import com.tuyn.demo.mvpdemo.ui.view.MainActivity;

public class FirstScreenNavigator extends BaseNavigator<MainActivity> {

    private static final String TAG = FirstScreenNavigator.class.getSimpleName();

    private DemoCollectionPagerAdapter mDemoAdapter;

    public FirstScreenNavigator(@NonNull MainActivity activity) {
        super(activity);
    }

    @Override
    public void start() {

    }

    @Override
    public void update() {
        for (Fragment fragment : getFragmentManager().getFragments()) {
            getActivity().providePresenter((BaseFragment) fragment);
        }
    }

}