package com.tuyn.demo.mvpdemo.ui.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tuyn.demo.mvpdemo.R;

public class DemoCollectionPagerAdapter extends FragmentPagerAdapter {
    DemoPagerFragmentProvider mProvider;

    public DemoCollectionPagerAdapter(FragmentManager fManager, DemoPagerFragmentProvider provider) {
        super(fManager);
        mProvider = provider;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = mProvider.getItem(i);
        Bundle args = new Bundle();
        args.putInt("args", i + 1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return mProvider.getFragmentCount();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "OBJECT " + (position + 1);
    }

}

