package com.tuyn.demo.mvpdemo.ui.base;

public interface BaseView<T extends BasePresenter> {
    void setPresenter(T presenter);

    void showProgress();

    default void showProgress(Double d){}

    void hideProgress();

    boolean isActive();
}
