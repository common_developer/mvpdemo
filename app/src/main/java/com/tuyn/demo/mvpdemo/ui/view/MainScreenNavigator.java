package com.tuyn.demo.mvpdemo.ui.view;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MenuItem;

import com.tuyn.demo.mvpdemo.R;
import com.tuyn.demo.mvpdemo.ui.adapter.DemoCollectionPagerAdapter;
import com.tuyn.demo.mvpdemo.ui.adapter.DemoPagerFragmentProvider;
import com.tuyn.demo.mvpdemo.ui.base.BaseFragment;
import com.tuyn.demo.mvpdemo.ui.base.BaseNavigator;

public class MainScreenNavigator extends BaseNavigator<MainActivity>
        implements NavigationView.OnNavigationItemSelectedListener,
        ViewPager.OnPageChangeListener {

    private static final String TAG = MainScreenNavigator.class.getSimpleName();


    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private DemoCollectionPagerAdapter mDemoAdapter;

    public MainScreenNavigator(@NonNull MainActivity activity,
                               @NonNull NavigationView view,
                               @NonNull ViewPager viewPager,
                               @NonNull TabLayout tabLayout) {
        super(activity);
        view.setNavigationItemSelectedListener(this);

        mViewPager = viewPager;
        mTabLayout = tabLayout;
        mTabLayout.setupWithViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(this);
        createViewPagerAdapter();
    }

    private void createViewPagerAdapter(){
        mDemoAdapter = new DemoCollectionPagerAdapter(getActivity().getSupportFragmentManager(), new DemoPagerFragmentProvider());
        mViewPager.setAdapter(mDemoAdapter);
    }


    @Override
    public void start() {

    }

    @Override
    public void update() {
        for (Fragment fragment : getFragmentManager().getFragments()) {
            getActivity().providePresenter((BaseFragment) fragment);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        getActivity().onNavigationUpdated();
        return true;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        Log.d(TAG, "onPageScrolled " + position);
    }

    @Override
    public void onPageSelected(int position) {
        Log.d(TAG, "onPageSelected " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        Log.d(TAG, "onPageScrollStateChanged " + state);

    }

    //    public void logOut() {
//        startActivity(LoginActivity.class);
//        getActivity().finish();
//    }

//    private void proceedToMyCars() {
//        getActivity().onNavigationUpdated(R.string.lbl_my_cars);
////        replaceAll(getActivity().provideMyCarsFragment(), R.id.content_frame, false);
//        showFragment(getActivity().provideMyCarsFragment(), R.id.content_frame);
//    }
//
//    private void proceedToProfile() {
//        getActivity().onNavigationUpdated(R.string.lbl_profile);
//        showFragment(getActivity().provideProfileFragment(), R.id.content_frame);
//    }
//
//    private void proceedToContacts() {
//        getActivity().onNavigationUpdated(R.string.lbl_contacts);
//        showFragment(getActivity().provideContactsFragment(), R.id.content_frame);
//    }
//
//    private void proceedToAccidents() {
//        getActivity().onNavigationUpdated(R.string.lbl_nav_accidents);
//        showFragment(getActivity().provideClaimsFragment(), R.id.content_frame);
//    }
//
//    private void proceedToPrivacy() {
//        getActivity().onNavigationUpdated(R.string.lbl_privacy);
//        showFragment(getActivity().providePrivacyFragment(), R.id.content_frame);
//    }
//
//    public void proceedToClaimInfo(Bundle data) {
//        startActivity(ClaimDetailsActivity.class, data);
//    }
//
//    public void proceedToNotifications() {
//        startActivity(NotificationsActivity.class);
//    }


}