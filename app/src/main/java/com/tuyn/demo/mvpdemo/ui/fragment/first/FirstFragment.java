package com.tuyn.demo.mvpdemo.ui.fragment.first;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.tuyn.demo.mvpdemo.R;
import com.tuyn.demo.mvpdemo.ui.base.BaseFragment;
import com.tuyn.demo.mvpdemo.ui.base.BaseRecyclerViewAdapter;
import com.tuyn.demo.mvpdemo.model.DemoModel;
import com.tuyn.demo.mvpdemo.ui.base.BaseViewHolderClickListener;
import com.tuyn.demo.mvpdemo.ui.base.ViewHolderFactory;
import com.tuyn.demo.mvpdemo.ui.factory.DemoViewHolderFactoryImpl;
import com.tuyn.demo.mvpdemo.ui.base.ViewTypeProvider;
import com.tuyn.demo.mvpdemo.ui.provider.DemoItemTypeProviderImpl;
import com.tuyn.demo.mvpdemo.ui.error.BaseUIError;
import com.tuyn.demo.mvpdemo.ui.view.firstactivity.FirstActivity;

public class FirstFragment extends BaseFragment implements FirstContract.View {

    private final ViewTypeProvider<DemoModel> viewTypeProvider = new DemoItemTypeProviderImpl();
    FirstContract.Presenter mPresenter;
    RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ViewHolderFactory<DemoModel> tViewHolderFactory;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = view.findViewById(R.id.recycler);
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        tViewHolderFactory = new DemoViewHolderFactoryImpl(getContext());

        BaseViewHolderClickListener<DemoModel> lst = new BaseViewHolderClickListener<DemoModel>() {
            @Override
            public void onClick(DemoModel item) {
                Log.e("CLICK", "" + item.firstName);
                getActivity().startActivity(new Intent(getActivity(), FirstActivity.class));
            }
        };

        mAdapter = new BaseRecyclerViewAdapter<>(DemoModel.getDummyList(), viewTypeProvider, tViewHolderFactory, lst);


        mRecyclerView.setAdapter(mAdapter);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(mDividerItemDecoration);

        mPresenter.create();
    }

    @Override
    public void onDestroyView() {
        mPresenter.destroy();
        super.onDestroyView();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_first; //Set fragment layout resource
    }

    @Override
    public void showError(BaseUIError error) {

    }

    @Override
    public void dismissDialog() {

    }

    @Override
    public void setPresenter(FirstContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public boolean isActive() {
        return isResumed();
    }
}
