package com.tuyn.demo.mvpdemo.ui.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import com.tuyn.demo.mvpdemo.BaseApplication;
import com.tuyn.demo.mvpdemo.R;

import java.lang.ref.WeakReference;

public abstract class BaseNavigator<T extends BaseActivity> {
    private final WeakReference<T> mActivity;

    public static final String IS_FROM_BG = BaseNavigator.class.getSimpleName() + ".IS_FROM_BG";

    private static final int ACTIVE_FRAGMENT_COUNT = 3;

    public BaseNavigator(T activity) {
        mActivity = new WeakReference<>(activity);
    }

    public abstract void start();

    public abstract void update();

    protected final FragmentManager getFragmentManager() {
        BaseActivity activity = getActivity();

        if (activity != null) {
            return activity.getSupportFragmentManager();
        }

        return null;
    }

    protected final void addFragmentToActivity(@NonNull BaseFragment fragment, int frameId) {
        FragmentManager manager = getFragmentManager();

        if (manager != null) {
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(frameId, fragment);
            transaction.commit();
        }
    }

    protected final void replaceAll(@NonNull BaseFragment fragment, int containerId, boolean addToBackStack) {
        FragmentManager manager = getFragmentManager();

        if (manager != null) {
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(containerId, fragment);

            if (addToBackStack) {
                transaction.addToBackStack(null);
            }

            transaction.commit();
        }
    }

    protected final void showFragment(@NonNull BaseFragment fragment, int containerId) {
        FragmentManager manager = getFragmentManager();

        if (manager != null) {
            FragmentTransaction transaction = manager.beginTransaction();
            BaseFragment frag = (BaseFragment) manager.findFragmentByTag(fragment.getClass().getSimpleName());

            int hiddenCnt = 0;
            int cnt = manager.getFragments().size();
            for (int i = cnt -1; i >= 0; i --){
                Fragment f = manager.getFragments().get(i);
                if(f.equals(frag)) {
                    hiddenCnt++;
                    continue;
                }
                if(hiddenCnt >= ACTIVE_FRAGMENT_COUNT) {
                    transaction.remove(f);
                } else {
                    transaction.hide(f);
                    hiddenCnt++;
                }
            }

            if(frag != null) {
                transaction.show(frag);
            } else {
                transaction.add(containerId, fragment, fragment.getClass().getSimpleName());
            }

            transaction.commit();
        }
    }

    public final void startExternalActivity(Intent intent) {
        Activity activity = getActivity();
        if(activity != null) {
            if (intent.resolveActivity(activity.getPackageManager()) != null) {
                activity.startActivity(intent);
            } else {
                Toast.makeText(activity, activity.getString(R.string.no_resolver), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public final void startExternalActivityForResult(Intent intent, int code) {
        Activity activity = getActivity();
        if(activity != null) {
            if (intent.resolveActivity(activity.getPackageManager()) != null) {
                activity.startActivityForResult(intent, code);
            } else {
                Toast.makeText(activity, activity.getString(R.string.no_resolver), Toast.LENGTH_SHORT).show();
            }
        }
    }

    protected final void finishActivity() {
        BaseActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    protected final void finishActivityWithResult(int resultCode) {
        BaseActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(resultCode);
            activity.finish();
        }
    }

    protected final void startActivity(Class cls) {
        startActivity(cls, null);
    }

    protected final void startActivity(Class cls, Bundle extras) {
        BaseActivity activity = getActivity();

        if (activity != null) {
            Intent intent = new Intent(activity, cls);
            intent.putExtra(IS_FROM_BG, BaseApplication.getLifecycleCallback().isInBackground());
            if (extras != null) intent.putExtras(extras);
            activity.startActivity(intent);
        }
    }

    protected final void startActivityForResult(Class cls, int requestCode) {
        startActivityForResultWithExtra(cls, requestCode, null);
    }

    protected final void startActivityForResultWithExtra(Class cls, int requestCode, Bundle extras) {
        BaseActivity activity = getActivity();

        if (activity != null) {
            Intent intent = new Intent(activity, cls);
            intent.putExtra(IS_FROM_BG, BaseApplication.getLifecycleCallback().isInBackground());
            if (extras != null) intent.putExtras(extras);
            activity.startActivityForResult(intent, requestCode);
        }
    }

    public final T getActivity() {
        T activity = mActivity.get();

        if (activity != null && activity.isFinishing()) {
            activity = null;
        }

        return activity;
    }
}