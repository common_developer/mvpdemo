package com.tuyn.demo.mvpdemo.ui;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

/**
 * Created by artashesv on 2/16/18.
 */

public final class ActivityLifecycleListener implements Application.ActivityLifecycleCallbacks {
    private static final String TAG = ActivityLifecycleListener.class.getSimpleName();
    private int refCount = 0;
    private boolean isInBackground = false;

    public final boolean isInBackground() {
        return isInBackground;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {
        if (++refCount == 1) {
            isInBackground = false;
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {
        if (--refCount == 0) {
            isInBackground = true;
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
