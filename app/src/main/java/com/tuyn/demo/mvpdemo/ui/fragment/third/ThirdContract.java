package com.tuyn.demo.mvpdemo.ui.fragment.third;

import com.tuyn.demo.mvpdemo.ui.base.BasePresenter;
import com.tuyn.demo.mvpdemo.ui.base.BaseView;
import com.tuyn.demo.mvpdemo.ui.error.BaseUIError;

public class ThirdContract {
    
    interface View extends BaseView<Presenter> {
        void showError(BaseUIError error);


        void dismissDialog();
    }

    interface Presenter extends BasePresenter {

        void getAllData();


    }
}