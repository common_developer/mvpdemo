package com.tuyn.demo.mvpdemo.ui.base;

import android.os.Bundle;

public interface BasePresenter {
    void create();

    void destroy();

    void saveInstanceState(Bundle bundle);

    void restoreInstanceState(Bundle bundle);
}
