package com.tuyn.demo.mvpdemo.ui.factory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tuyn.demo.mvpdemo.R;
import com.tuyn.demo.mvpdemo.model.DemoModel;
import com.tuyn.demo.mvpdemo.ui.base.BaseViewHolder;
import com.tuyn.demo.mvpdemo.ui.base.BaseViewHolderClickListener;
import com.tuyn.demo.mvpdemo.ui.base.ViewHolderFactory;
import com.tuyn.demo.mvpdemo.ui.provider.DemoItemViewType;
import com.tuyn.demo.mvpdemo.ui.viewholder.DemoViewHolder;

public class DemoViewHolderFactoryImpl implements ViewHolderFactory<DemoModel> {
    private final LayoutInflater layoutInflater;

    public DemoViewHolderFactoryImpl(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public BaseViewHolder<DemoModel> createViewHolder(int viewType, ViewGroup parent, BaseViewHolderClickListener<DemoModel> listener) {
        BaseViewHolder<DemoModel> result;
        switch (viewType) {
            case DemoItemViewType.KLOR_VIEW_TYPE: {
                View view = layoutInflater.inflate(R.layout.viewholder_demo, parent, false);
                result = new DemoViewHolder(view, listener);
                break;
            }
            default: {
                result = null;
                throw new IllegalArgumentException("some message for debugging");
            }
        }
        return result;
    }
}
