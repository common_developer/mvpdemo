package com.tuyn.demo.mvpdemo.model;

import android.content.Context;

public abstract class ToDoDatabase  {

        private static ToDoDatabase INSTANCE;

        public abstract Object taskDao();

        private static final Object sLock = new Object();

        public static ToDoDatabase getInstance(Context context) {
            synchronized (sLock) {
                if (INSTANCE == null) {
//                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
//                            ToDoDatabase.class, "Tasks.db")
//                            .build();
                }
                return INSTANCE;
            }
        }

    }