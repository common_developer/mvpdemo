package com.tuyn.demo.mvpdemo.ui.provider;

import com.tuyn.demo.mvpdemo.model.DemoModel;
import com.tuyn.demo.mvpdemo.ui.base.ViewTypeProvider;

public class DemoItemTypeProviderImpl implements ViewTypeProvider<DemoModel> {
    @Override
    public int provideViewType(DemoModel t) {
        @DemoItemViewType int result = t.getType();
        return result;
    }
}
