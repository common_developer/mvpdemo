package com.tuyn.demo.mvpdemo.ui.viewholder;

import android.view.View;
import android.widget.TextView;

import com.tuyn.demo.mvpdemo.R;
import com.tuyn.demo.mvpdemo.model.DemoLeftRightModel;
import com.tuyn.demo.mvpdemo.model.DemoModel;
import com.tuyn.demo.mvpdemo.ui.base.BaseViewHolder;
import com.tuyn.demo.mvpdemo.ui.base.BaseViewHolderClickListener;

public class DemoLeftRightViewHolder extends BaseViewHolder<DemoLeftRightModel> {
    TextView name;
    TextView message;
    BaseViewHolderClickListener<DemoLeftRightModel> mListener;

    public DemoLeftRightViewHolder(View v, BaseViewHolderClickListener<DemoLeftRightModel> listener) {
        super(v);
        mListener = listener;
        name = v.findViewById(R.id.name);
        message = v.findViewById(R.id.message);

    }

    @Override
    public void bindViewHolder(final DemoLeftRightModel item) {
        name.setText(item.firstName + " " + item.lastName);
        message.setText(item.message);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(item);
            }
        });
    }
}
