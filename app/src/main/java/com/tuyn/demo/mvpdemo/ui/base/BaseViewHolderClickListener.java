package com.tuyn.demo.mvpdemo.ui.base;

public abstract class BaseViewHolderClickListener<T> {
    public abstract void onClick(T item);
}
