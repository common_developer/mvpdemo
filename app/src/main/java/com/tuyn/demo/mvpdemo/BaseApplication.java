package com.tuyn.demo.mvpdemo;

import android.app.Application;
import android.os.StrictMode;

import com.tuyn.demo.mvpdemo.ui.ActivityLifecycleListener;


/**
 * Created by artashesv on 2/8/18.
 */

public final class BaseApplication extends Application {
    private static BaseApplication sApp;
    private static ActivityLifecycleListener lifecycleCallback;

    @Override
    public void onCreate() {

        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()   // or .detectAll() for all detectable problems
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());
        }

        super.onCreate();
        sApp = this;
        lifecycleCallback = new ActivityLifecycleListener();
        this.registerActivityLifecycleCallbacks(lifecycleCallback);

    }


    public static BaseApplication getApplication() {
        return sApp;
    }

    public static ActivityLifecycleListener getLifecycleCallback() {
        return lifecycleCallback;
    }

}
