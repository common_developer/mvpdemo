package com.tuyn.demo.mvpdemo.ui.viewholder;

import android.view.View;
import android.widget.TextView;

import com.tuyn.demo.mvpdemo.R;
import com.tuyn.demo.mvpdemo.ui.base.BaseViewHolder;
import com.tuyn.demo.mvpdemo.model.DemoModel;
import com.tuyn.demo.mvpdemo.ui.base.BaseViewHolderClickListener;

public class DemoViewHolder extends BaseViewHolder<DemoModel> {
    TextView firstName;
    TextView lastName;
    BaseViewHolderClickListener<DemoModel> mListener;

    public DemoViewHolder(View v, BaseViewHolderClickListener<DemoModel> listener) {
        super(v);
        mListener = listener;
        firstName = v.findViewById(R.id.first_name);
        lastName = v.findViewById(R.id.last_name);
    }

    @Override
    public void bindViewHolder(final DemoModel item) {
        firstName.setText(item.firstName);
        lastName.setText(item.lastName);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(item);
            }
        });
    }
}
