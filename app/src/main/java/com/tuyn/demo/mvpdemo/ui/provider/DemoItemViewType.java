package com.tuyn.demo.mvpdemo.ui.provider;

import android.support.annotation.IntDef;

@IntDef({DemoItemViewType.KLOR_VIEW_TYPE})
public @interface DemoItemViewType {
    int KLOR_VIEW_TYPE = 0;
    //....etc
}
