package com.tuyn.demo.mvpdemo.ui.fragment.second;

import android.os.Bundle;
import android.support.annotation.NonNull;

public class SecondPresenter implements SecondContract.Presenter {


    private final SecondContract.View mMainView;

    public SecondPresenter(@NonNull SecondContract.View mainView) {
        this.mMainView = mainView;
    }

    @Override
    public void create() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void saveInstanceState(Bundle bundle) {

    }

    @Override
    public void restoreInstanceState(Bundle bundle) {

    }

    @Override
    public void getAllData() {

    }
}