package com.tuyn.demo.mvpdemo.ui.fragment.third;

import android.os.Bundle;
import android.support.annotation.NonNull;

public class ThirdPresenter implements ThirdContract.Presenter {


    private final ThirdContract.View mMainView;

    public ThirdPresenter(@NonNull ThirdContract.View mainView) {
        this.mMainView = mainView;
    }

    @Override
    public void create() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void saveInstanceState(Bundle bundle) {

    }

    @Override
    public void restoreInstanceState(Bundle bundle) {

    }

    @Override
    public void getAllData() {

    }
}