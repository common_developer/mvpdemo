package com.tuyn.demo.mvpdemo.ui.fragment.fourth;

import android.os.Bundle;
import android.support.annotation.NonNull;

public class FourthPresenter implements FourthContract.Presenter {


    private final FourthContract.View mMainView;

    public FourthPresenter(@NonNull FourthContract.View mainView) {
        this.mMainView = mainView;
    }

    @Override
    public void create() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void saveInstanceState(Bundle bundle) {

    }

    @Override
    public void restoreInstanceState(Bundle bundle) {

    }

    @Override
    public void getAllData() {

    }

    @Override
    public void sendMessage(String message) {
        //Logic to send message
        mMainView.messageSent(message);
    }
}