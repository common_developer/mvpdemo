package com.tuyn.demo.mvpdemo.ui.base;

public interface ViewTypeProvider<T> {
    int provideViewType(T t);
}
