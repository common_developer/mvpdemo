package com.tuyn.demo.mvpdemo.ui.fragment.third;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.tuyn.demo.mvpdemo.R;
import com.tuyn.demo.mvpdemo.ui.base.BaseFragment;
import com.tuyn.demo.mvpdemo.ui.error.BaseUIError;

public class ThirdFragment extends BaseFragment implements  ThirdContract.View {

    ThirdContract.Presenter mPresenter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.create();
    }

    @Override
    public void onDestroyView() {
        mPresenter.destroy();
        super.onDestroyView();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_third; //Set fragment layout resource
    }

    @Override
    public void showError(BaseUIError error) {

    }

    @Override
    public void dismissDialog() {

    }

    @Override
    public void setPresenter(ThirdContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public boolean isActive() {
        return isResumed();
    }
}
