package com.tuyn.demo.mvpdemo.ui.fragment.fourth;

import com.tuyn.demo.mvpdemo.ui.base.BasePresenter;
import com.tuyn.demo.mvpdemo.ui.base.BaseView;
import com.tuyn.demo.mvpdemo.ui.error.BaseUIError;

public class FourthContract {
    
    interface View extends BaseView<Presenter> {
        void showError(BaseUIError error);


        void dismissDialog();

        void messageSent(String message);
    }

    interface Presenter extends BasePresenter {

        void getAllData();

        void sendMessage(String message);


    }
}