package com.tuyn.demo.mvpdemo.ui.view.firstactivity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.tuyn.demo.mvpdemo.R;
import com.tuyn.demo.mvpdemo.ui.base.BaseActivity;
import com.tuyn.demo.mvpdemo.ui.base.BaseFragment;
import com.tuyn.demo.mvpdemo.ui.fragment.fourth.FourthFragment;
import com.tuyn.demo.mvpdemo.ui.fragment.fourth.FourthPresenter;

public class FirstActivity extends BaseActivity {

    //The identifier does not have to be unique in this view's hierarchy. The identifier should be a positive number.
    private static final int CONTENT_VIEW_ID = 1234;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FrameLayout frame = new FrameLayout(this);
        frame.setId(CONTENT_VIEW_ID);
        setContentView(frame, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

        if (savedInstanceState == null) {
            FourthFragment newFragment = new FourthFragment();
            FourthPresenter sp = new FourthPresenter(newFragment);
            newFragment.setPresenter(sp);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

            ft.add(CONTENT_VIEW_ID, newFragment).commit();
        }else{
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                providePresenter((BaseFragment) fragment);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void providePresenter(BaseFragment fragment) {
        if(fragment instanceof FourthFragment){
            ((FourthFragment) fragment).setPresenter(new FourthPresenter((FourthFragment) fragment));
        }
    }
}
