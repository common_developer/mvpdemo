package com.tuyn.demo.mvpdemo.ui.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {
    public BaseViewHolder(View v) {
        super(v);
    }

    public abstract void bindViewHolder(T item);
}