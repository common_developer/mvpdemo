package com.tuyn.demo.mvpdemo.ui.base;

import android.view.ViewGroup;

public interface ViewHolderFactory<T> {
    BaseViewHolder<T> createViewHolder(int viewType, ViewGroup parent, BaseViewHolderClickListener<T> listener);
}
