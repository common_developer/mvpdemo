package com.tuyn.demo.mvpdemo.ui.factory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tuyn.demo.mvpdemo.R;
import com.tuyn.demo.mvpdemo.model.DemoLeftRightModel;
import com.tuyn.demo.mvpdemo.model.DemoModel;
import com.tuyn.demo.mvpdemo.ui.base.BaseViewHolder;
import com.tuyn.demo.mvpdemo.ui.base.BaseViewHolderClickListener;
import com.tuyn.demo.mvpdemo.ui.base.ViewHolderFactory;
import com.tuyn.demo.mvpdemo.ui.provider.DemoItemViewType;
import com.tuyn.demo.mvpdemo.ui.provider.DemoLeftRightItemViewType;
import com.tuyn.demo.mvpdemo.ui.viewholder.DemoLeftRightViewHolder;
import com.tuyn.demo.mvpdemo.ui.viewholder.DemoViewHolder;

public class DemoLeftRightViewHolderFactoryImpl implements ViewHolderFactory<DemoLeftRightModel> {
    private final LayoutInflater layoutInflater;

    public DemoLeftRightViewHolderFactoryImpl(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public BaseViewHolder<DemoLeftRightModel> createViewHolder(int viewType, ViewGroup parent, BaseViewHolderClickListener<DemoLeftRightModel> listener) {
        BaseViewHolder<DemoLeftRightModel> result;
        switch (viewType) {
            case DemoLeftRightItemViewType.LEFT_ITEM: {
                View view = layoutInflater.inflate(R.layout.viewholder_left_demo, parent, false);
                result = new DemoLeftRightViewHolder(view, listener);
                break;
            }
            case DemoLeftRightItemViewType.RIGHT_ITEM: {
                View view = layoutInflater.inflate(R.layout.viewholder_right_demo, parent, false);
                result = new DemoLeftRightViewHolder(view, listener);
                break;
            }
            default: {
                result = null;
                throw new IllegalArgumentException("some message for debugging");
            }
        }
        return result;
    }
}
