package com.tuyn.demo.mvpdemo.ui.error;

import android.support.annotation.IntDef;
import android.support.annotation.StringRes;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

public class BaseUIError {

    @Retention(SOURCE)
    @IntDef({SHORT_TOAST, LONG_TOAST, DIALOG, EDIT_TEXT, INLINE})
    public @interface MessageType {
    }

    public static final int SHORT_TOAST = 0;
    public static final int LONG_TOAST = 1;
    public static final int DIALOG = 2;
    public static final int EDIT_TEXT = 3;
    public static final int INLINE = 4;

    public final int kind;
    public final @MessageType
    int messageType;
    public final @StringRes
    int errorTitleRes;
    public final @StringRes
    int errorMessageRes;

    protected BaseUIError(int kind,
                          @MessageType int messageType,
                          @StringRes int errorTitleRes,
                          @StringRes int errorMessageRes) {
        this.kind = kind;
        this.messageType = messageType;
        this.errorTitleRes = errorTitleRes;
        this.errorMessageRes = errorMessageRes;
    }
}