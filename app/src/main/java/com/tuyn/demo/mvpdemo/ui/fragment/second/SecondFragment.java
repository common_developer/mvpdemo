package com.tuyn.demo.mvpdemo.ui.fragment.second;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.tuyn.demo.mvpdemo.R;
import com.tuyn.demo.mvpdemo.ui.base.BaseFragment;
import com.tuyn.demo.mvpdemo.ui.error.BaseUIError;

public class SecondFragment extends BaseFragment implements  SecondContract.View {

    SecondContract.Presenter mPresenter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.create();
    }

    @Override
    public void onDestroyView() {
        mPresenter.destroy();
        super.onDestroyView();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_second; //Set fragment layout resource
    }

    @Override
    public void showError(BaseUIError error) {

    }

    @Override
    public void dismissDialog() {

    }

    @Override
    public void setPresenter(SecondContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public boolean isActive() {
        return isResumed();
    }
}
