package com.tuyn.demo.mvpdemo.ui.provider;

import android.support.annotation.IntDef;

@IntDef({DemoLeftRightItemViewType.LEFT_ITEM, DemoLeftRightItemViewType.RIGHT_ITEM})
public @interface DemoLeftRightItemViewType {
    int LEFT_ITEM = 0;
    int RIGHT_ITEM = 1;
    //....etc
}
