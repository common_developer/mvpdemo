package com.tuyn.demo.mvpdemo.ui.provider;

import com.tuyn.demo.mvpdemo.model.DemoLeftRightModel;
import com.tuyn.demo.mvpdemo.model.DemoModel;
import com.tuyn.demo.mvpdemo.ui.base.ViewTypeProvider;

public class DemoLeftRightTypeProviderImpl implements ViewTypeProvider<DemoLeftRightModel> {
    @Override
    public int provideViewType(DemoLeftRightModel t) {
        @DemoLeftRightItemViewType int result = t.getType();
        return result;
    }
}
