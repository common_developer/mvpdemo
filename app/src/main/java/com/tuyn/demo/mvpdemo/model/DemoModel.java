package com.tuyn.demo.mvpdemo.model;

import com.tuyn.demo.mvpdemo.ui.provider.DemoItemViewType;

import java.util.ArrayList;
import java.util.List;

public class DemoModel {
    @DemoItemViewType
    private final int type = DemoItemViewType.KLOR_VIEW_TYPE;

    public String firstName;
    public String lastName;

    public DemoModel(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @DemoItemViewType
    public int getType() {
        return type;
    }

    public static List<DemoModel> getDummyList(){
        List<DemoModel> lst = new ArrayList<>();
        lst.add(new DemoModel("asd", "dsa"));
        lst.add(new DemoModel("wer", "rew"));
        lst.add(new DemoModel("tyu", "uyt"));
        lst.add(new DemoModel("ghj", "jhg"));
        lst.add(new DemoModel("qwe", "ewq"));
        lst.add(new DemoModel("bnm", "mnb"));
        lst.add(new DemoModel("hjk", "kjh"));
        lst.add(new DemoModel("iop", "poi"));
        lst.add(new DemoModel("zxc", "cxz"));
        lst.add(new DemoModel("fgh", "hgf"));
        return lst;
    }
}
