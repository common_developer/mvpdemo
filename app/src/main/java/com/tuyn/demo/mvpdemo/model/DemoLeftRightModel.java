package com.tuyn.demo.mvpdemo.model;

import com.tuyn.demo.mvpdemo.ui.provider.DemoItemViewType;
import com.tuyn.demo.mvpdemo.ui.provider.DemoLeftRightItemViewType;

import java.util.ArrayList;
import java.util.List;

public class DemoLeftRightModel {

    public String firstName;
    public String lastName;
    public String message;
    public Boolean left;

    public DemoLeftRightModel(String firstName, String lastName, String message, boolean left) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.message = message;
        this.left = left;
    }

    @DemoLeftRightItemViewType
    public int getType() {
        if(left)
            return DemoLeftRightItemViewType.LEFT_ITEM;
        else
            return DemoLeftRightItemViewType.RIGHT_ITEM;
    }

    public static List<DemoLeftRightModel> getDummyList(){
        List<DemoLeftRightModel> lst = new ArrayList<>();
        lst.add(new DemoLeftRightModel("asd", "dsa", "1", true));
        lst.add(new DemoLeftRightModel("wer", "rew", "2", true));
        lst.add(new DemoLeftRightModel("tyu", "uyt", "3", false));
        lst.add(new DemoLeftRightModel("ghj", "jhg", "4", true));
        lst.add(new DemoLeftRightModel("qwe", "ewq", "5", false));
        lst.add(new DemoLeftRightModel("bnm", "mnb", "6", true));
        lst.add(new DemoLeftRightModel("hjk", "kjh", "7", false));
        lst.add(new DemoLeftRightModel("iop", "poi", "8", false));
        lst.add(new DemoLeftRightModel("zxc", "cxz", "9", true));
        lst.add(new DemoLeftRightModel("fgh", "hgf", "10", false));
        lst.add(new DemoLeftRightModel("fgh", "hgf", "11", true));
        lst.add(new DemoLeftRightModel("fgh", "hgf", "12", false));
        lst.add(new DemoLeftRightModel("fgh", "hgf", "13", true));
        lst.add(new DemoLeftRightModel("asd", "dsa", "14", true));
        lst.add(new DemoLeftRightModel("wer", "rew", "15", true));
        lst.add(new DemoLeftRightModel("tyu", "uyt", "16", false));
        lst.add(new DemoLeftRightModel("ghj", "jhg", "17", true));
        lst.add(new DemoLeftRightModel("qwe", "ewq", "18", false));
        lst.add(new DemoLeftRightModel("bnm", "mnb", "19", true));
        lst.add(new DemoLeftRightModel("hjk", "kjh", "20", false));

        return lst;
    }
}
